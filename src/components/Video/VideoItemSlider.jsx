import React from "react"
import { Link } from 'react-router-dom';
import imgTuDefault from "../../assets/img/doc-co-cau-bai.jpg";
import moment from 'moment'


const VideoItemSlider = ({tutorial}) => (
    
        <div className="slider-item " key={tutorial.id}>
            <div className = "slider-thumb">
                <div className ="thumb-box">
                    <Link to={'/watch/'+tutorial.shortName+'_.'+tutorial.id}>
                        <img src= {tutorial.avatar ? tutorial.avatar :imgTuDefault} alt="edu" />
                    </Link>
                    <div className = "video-time-play">
                        <span className = "time-play-status">45:20</span>
                    </div>
                    <div className ="vi-note">
                        <span className ="vi-note-sub">Live</span>
                    </div>
                </div>
                
                <div className="slider-vi-detail">
                    <div className="slider-vi-title ">
                            <Link to={'/watch/'+tutorial.shortName+'_.'+tutorial.id} ><h3 className="vi-title">{tutorial.name}</h3></Link>
                            <Link to={'/edu/'+tutorial.eduShortName} ><h4 className="edu-name">{tutorial.eduName}</h4></Link>
                    </div>
                    <div className = "slider-vi-meta">
                        <span className = "date-meta">{moment(tutorial.createdAt).fromNow()}</span>
                        <span className = "view-meta">{tutorial.viewTotal} <i className="ti-eye"></i> </span>
                    </div>
                </div>
            </div>
        </div>    
    
);

export default VideoItemSlider