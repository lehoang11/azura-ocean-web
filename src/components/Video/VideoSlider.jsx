import React from "react"
import { Link } from 'react-router-dom';
import VideoItemSlider from "./VideoItemSlider";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import imgTuDefault from "../../assets/img/doc-co-cau-bai.jpg";
import moment from 'moment'

const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

class VideoSlider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render () {
        return (
                <Carousel responsive={responsive}>
                    {this.props.tutorials.map((tutorial, i)=> {
                        return (
                            <div className="slider-item " key={tutorial.id}>
                                <div className = "slider-thumb">
                                    <div className ="thumb-box">
                                        <Link to={'/watch/'+tutorial.shortName+'_.'+tutorial.id}>
                                            <img src= {tutorial.avatar ? tutorial.avatar :imgTuDefault} alt="edu" />
                                        </Link>
                                        <div className = "video-time-play">
                                            <span className = "time-play-status">45:20</span>
                                        </div>
                                        <div className ="vi-note">
                                            <span className ="vi-note-sub">Live</span>
                                        </div>
                                    </div>
                                    
                                    <div className="slider-vi-detail">
                                        <div className="slider-vi-title ">
                                            <Link to={'/watch/'+tutorial.shortName+'_.'+tutorial.id} ><h3 className="vi-title">{tutorial.name} toi la toi em la em</h3></Link>
                                            <Link to={'/edu/'+tutorial.eduShortName} ><h4 className="edu-name">{tutorial.eduName}</h4></Link>
                                        </div>
                                        <div className = "slider-vi-meta">
                                            <span className = "date-meta">{moment(tutorial.createdAt).fromNow()}</span>
                                            <span className = "view-meta">{tutorial.viewTotal} <i className="ti-eye"></i> </span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            );
                        })
                    }
                </Carousel>      
        );
    }
}
export default VideoSlider;