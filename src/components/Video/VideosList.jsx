import React from "react"
import { Link } from 'react-router-dom';
import VideoItem from "./VideoItem"

class VideosList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render () {
        return (
            <section className="list-video-show">
                {this.props.tutorials.map((tutorial, i)=> {
                    return (
                        <VideoItem key={i} tutorial={tutorial} />
                    );
                    })
                }
            </section>
        );
    }
}
export default VideosList;