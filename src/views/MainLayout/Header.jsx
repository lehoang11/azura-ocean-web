import React from "react";
import { Link } from 'react-router-dom';
import userAvatar from "../../assets/img/varun.jpg";
import history from "../../helpers/history";
import userDefault from '../../assets/img/user-default.png'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.input = null;
        this.state = {};
    }

    componentDidMount() {
        document.addEventListener('keydown', this.onKeyDown, false);
    }
    
    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDown, false);
    }
    
    onKeyDown(e) {
        if (e.keyCode === 191) {
          const insideInput = e.target.tagName.toLowerCase().match(/input|textarea/);
          if (!insideInput) {
            e.preventDefault();
            this.input.focus();
          }
        }
    }

    onKeyPress(e) {
        if (e.charCode === 13) {
            const value = e.currentTarget.value.trim();
            if (value !== '') {
            // redirect
            history.push('/search?q='+value);
            }
        }
    }
    

    render () {
        const { user } = this.props;
        return (
            <div id="header">
                <div className="container">
                    <div className="row" id="header_wrap">

                        <div className="col-md-3 col-sm-6 slogan">
                            <div className="logo">
                                <a href="#" className="logo-text">azura</a>
                            </div>
                        </div>

                        <div className="col-md-5 col-sm-6 app-search-form hidden-xs">
                            <div className="header-nav">
                                <div className="col-xs-12">
                                
                                    <div className="form-group">
                                        <div className="input-group col-xs-12">
                                        <input 
                                        ref={(node) => { this.input = node; }}
                                        id="search" 
                                        onKeyPress={this.onKeyPress}
                                        type="text" 
                                        className="form-control" 
                                        placeholder="Search..." 
                                        autocomplete="off" required="" />
                                        {/* <i className="animate-spin hl-spin4 hidden"></i> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    

                        <div className="col-md-4 hidden-xs">

                        <div className="nav-item upload-link box-shadow">
                            <div className="dropdown">
                                <a href="javascript:;" className="avt" id="uploadLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                     <span><i className="ti-upload mr-1 ml-1"></i> upload</span>
                                    <i className="hl-down-open-mini"></i>
                                </a>
                                <ul className="dropdown-menu login-box" aria-labelledby="uploadLink">
                                    <li className="nav-link box-shadow">
                                    <Link rel="nofollow" to="/upload" title="upload media"> <i className="ti-layout-tab-window mr-1 ml-1"></i> Upload <span>video or mp3</span>
                                    </Link>
                                    </li>
                                    <li className="nav-link box-shadow">
                                    <Link rel="nofollow" to="/openClass" title="openClass"><i className="ti-rss-alt mr-1 ml-1"></i> Open <span>treaming</span>
                                    </Link>
                                    </li>
                                </ul>
                                </div>
                        </div>

                            <div className="nav-item user-login box-shadow">
                                <div className="dropdown">
                               
                                { user ? (<div>  
                                    <Link to="/login" className="avt" id="userInfo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src={userDefault} className="avatar avatar-20 photo avatar-default" height="20" width="20" /> <span class="name">{user.username}</span>  
                                    </Link>
                                    <ul className="dropdown-menu login-box" aria-labelledby="userInfo">
                                    {user.edu ? (
                                        <li className="nav-link box-shadow">
                                            <Link rel="nofollow" to={'/edu/'+user.edu.shortName} title="My Edu"><i className="ti-briefcase mr-1 ml-1"></i> My <span>edu</span>
                                            </Link>
                                        </li>
                                        ) :(
                                            <li className="nav-link box-shadow">
                                                <Link rel="nofollow" to={'/edu_create'}> title="Create Edu"><i className="ti-wallet mr-1 ml-1"></i> Create <span>an Edu</span>
                                                </Link>
                                            </li>    
                                        )
                                    }
                                        <li className="nav-link box-shadow">
                                            <Link rel="nofollow" to="#" title="Profile"><i className="ti-user mr-1 ml-1"></i> My <span> Profile</span>
                                            </Link>
                                        </li>
                                        <li className="nav-link box-shadow">
                                        <Link rel="nofollow" to="/login" title="Login"><i className="fa fa-power-off mr-1 ml-1"></i> Login 
                                        </Link>
                                        </li>
                                    </ul>
                                    </div>):(<div>
                                        <Link to="/login" className="avt" id="userInfo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <img src={userDefault} className="avatar avatar-20 photo avatar-default" height="20" width="20" /> <span class="name">Login</span>  
                                        </Link>
                                    </div>) 
                                } 
                                
                                </div>
                            </div>
                        </div>

                        

                    </div>
                </div>
            </div>
            
    
        );
    }

}


export default Header;
