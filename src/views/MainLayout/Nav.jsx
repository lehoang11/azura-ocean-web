import React from "react";
import { Link } from 'react-router-dom';

// import { connect } from 'react-redux';

class Nav extends React.Component {

    render () {
        return (
            <div className="navbar-container">
                <div className="container">
                    <nav className="navbar main-navigation" role="navigation" data-dropdown-hover="1">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
                            <span className="sr-only">menu</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            </button>
                        </div>
                        <div className="collapse navbar-collapse" id="mainNav">
                            <div className="main-nav-container">
                                <ul id="main-menu-nav" className="nav navbar-nav navbar-left">
                                    <li className="current-menu-item active"><a title="Trang chủ" href="#">Trang chủ</a></li>
                                    <li><a title="Phim mới" href="#">Phim mới</a></li>
                                    <li><a title="Phim bộ" href="#">Phim bộ</a></li>
                                    <li><a title="Phim lẻ" href="#">Phim lẻ</a></li>
                                    <li className="mega dropdown">
                                        <a title="Thể loại" href="#" data-toggle="dropdown" className="dropdown-toggle" aria-haspopup="true">Thể loại <span className="caret"></span></a>
                                        <ul role="menu" className=" dropdown-menu">
                                            <li><a title="Khoa Học - Viễn Tưởng" href="#">Khoa Học &#8211; Viễn Tưởng</a></li>
                                            <li><a title="Hình Sự - Chiến Tranh" href="#">Hình Sự &#8211; Chiến Tranh</a></li>
                                            <li><a title="Khoa Học - Viễn Tưởng" href="#">Khoa Học &#8211; Tâm lý</a></li>
                                            <li><a title="Hình Sự - Chiến Tranh" href="#">Hình Sự &#8211; Hành động</a></li>
                                        </ul>
                                    </li>
                                    <li className="mega dropdown">
                                        <a title="Quốc gia" href="#" data-toggle="dropdown" className="dropdown-toggle" aria-haspopup="true">Quốc gia <span className="caret"></span></a>
                                        <ul role="menu" className=" dropdown-menu">
                                            <li><a title="Trung Quốc" href="#">Trung Quốc</a></li>
                                            <li><a title="Hàn Quốc" href="#">Hàn Quốc</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </div> 
                        </div>
                    </nav>
                </div>
            </div>
        );
    }
}

export default Nav;