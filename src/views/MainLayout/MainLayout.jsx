import React, { Component }   from 'react';
import Header  from "./Header";
import Nav  from "./Nav";
import { connect } from 'react-redux';
class MainLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render(){
        const { user } = this.props;
        //console.log(user)
        return (
            <div>
                <div id="main-wrapper">
                    <Header user = {user} />
                    <Nav />
                    {this.props.children}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user :state.userReducer.user
  });

export default connect(mapStateToProps)(MainLayout);