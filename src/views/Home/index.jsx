import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import "../../assets/css/video.css"
import VideoList from "../../components/Video/VideosList"
import VideoSlider from "../../components/Video/VideoSlider"
import TutorialAPI from '../../api/tutorialApi'
import LoadingBar from 'react-top-loading-bar'
import moment from 'moment'
import imgTuDefault from "../../assets/img/doc-co-cau-bai.jpg";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tutorials: [],
            isLoading: true,
            errors: null,
            loadingBarProgress: 0
          };
    }

    complete = () => {
        this.setState({ loadingBarProgress: 100 })
    }
    
    onLoaderFinished = () => {
        this.setState({ loadingBarProgress: 0 })
    }


    getTutorialList = () => {
        return TutorialAPI.getTutorialList().then(
            (res) => {
                if (res.data.code == 200) {
                    this.setState({
                        tutorials: res.data.data.content,
                        isLoading: false,
                    })
                    this.complete()
                }
            },
            (err) => {
                console.log("error get data");
                this.setState({
                    isLoading: true,
                    errors :err,
                })
            }
        )

    }

    componentWillMount(){
    }

    componentDidMount() {
        this.getTutorialList();

    }
    


    render() {
        const { user } = this.props;
        const { isLoading, tutorials } = this.state;
        return (
        <div>
            <LoadingBar
            progress={this.state.loadingBarProgress}
            height={3}
            color="red"
            onLoaderFinished={() => this.onLoaderFinished()}
            />
            <div className="container">
                <div className="row container" id="mainContent">
                    <main className="col-xs-12">
                        <div className="page-content">

                        <div className="wrapSliderStyle conteiner-sub-001">
                            <div className="section-heading">
                                <Link to="#"><span>Đề xuất</span></Link>
                            </div>
                            {!isLoading ? <div><VideoSlider tutorials={tutorials} /></div> :<p>Loading...</p>}
                        </div>

                        <div className="wrapSliderStyle conteiner-sub-001">
                            <div className="section-heading">
                                <Link to="#"><span>Live streaming</span></Link>
                            </div>
                            {!isLoading ? <div><VideoSlider tutorials={tutorials} /></div> :<p>Loading...</p>}
                        </div>
                        
                            

    
                            <div className="wrapContainer conteiner-sub-005">
                                <div className="section-heading">
                                    <Link to="#"><span>Đề Xuất</span></Link>
                                </div>
                                <div className="wedget-005">
                                {!isLoading ? <div><VideoList tutorials={tutorials} /></div> :<p>Loading...</p>}
                                </div>
                            </div>

                        </div>
                    </main>
                </div>
            </div>
        </div>
        );
    }
}

const mapStateToProps = state => ({
    user :state.userReducer.user
  });

export default connect(mapStateToProps)(Home);