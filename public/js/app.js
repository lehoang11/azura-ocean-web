// slider
$( document ).ready(function() {				
    var owl = $('.olwControll');
    owl.owlCarousel({
        loop: true,
        margin: 5,
        autoplay: false,
        autoplayTimeout: 4000,
        autoplayHoverPause: false,
        nav: false,
        navText: ['<i class=" ti-angle-left"></i>',
         '<i class=" ti-angle-right"></i>'],
         responsiveClass: true,
         responsive: {
            0: {items:2},
            480: {items:3}, 
            600: {items:4},
            1000: {items: 5}}
        })
    });
    
